export const CHAIN_CONFIG = () => {
  const environment = process.env.ENVIRONMENT;
  const nodeUrl = process.env.NODE_URL;
  const secondaryNode = process.env.BACKUP_NODE_URL;
  if (environment && environment.toLowerCase() === 'development') {
    return {
      chainId:
        'b64646740308df2ee06c6b72f34c0f7fa066d940e831f752db2006fcc2b78dee',
      hyperion: {
        url: nodeUrl || 'https://testnet.libre.org',
        swapContract: 'evotest',
        backup_url: 'https://hyperion.libretest.quantumblok.com',
      },
      core: {
        url: 'https://server.staging.bitcoinlibre.io',
      },
      btc: {
        symbol: 'BTCL',
        contract: 'eosio.token',
        precision: 8,
      },
      usdt: {
        symbol: 'USDL',
        contract: 'eosio.token',
        precision: 8,
      },
      libre: {
        symbol: 'LIBRE',
        contract: 'eosio.token',
        precision: 4,
      },
      btcusd: {
        precision: 7,
      },
    };
  }
  return {
    chainId: '38b1d7815474d0c60683ecbea321d723e83f5da6ae5f1c1f9fecc69d9ba96465',
    hyperion: {
      url: nodeUrl || 'https://lb.libre.org',
      swapContract: 'swap.libre',
      backup_url: secondaryNode || 'https://lb.libre.org',
    },
    core: {
      url: 'https://server.production.bitcoinlibre.io',
    },
    btc: {
      symbol: 'PBTC',
      contract: 'btc.ptokens',
      precision: 9,
    },
    usdt: {
      symbol: 'PUSDT',
      contract: 'usdt.ptokens',
      precision: 9,
    },
    libre: {
      symbol: 'LIBRE',
      contract: 'eosio.token',
      precision: 4,
    },
    btcusd: {
      precision: 9,
    },
  };
};
