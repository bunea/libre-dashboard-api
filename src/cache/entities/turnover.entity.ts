import {
  Entity,
  Column,
  OneToMany,
  ManyToOne,
  ManyToMany,
  Generated,
} from 'typeorm';
import { BaseEntity } from '../../core/entities/base.entity';

@Entity({ name: 'turnover_stats' })
export class Turnover extends BaseEntity {
  constructor(partial: Partial<Turnover>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'varchar', length: 200, nullable: true })
  date: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  pool: string;

  @Column({ type: 'numeric', nullable: true })
  pool_1: number;

  @Column({ type: 'numeric', nullable: true })
  pool_2: number;
}
