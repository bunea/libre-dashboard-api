import { Controller, Get, Param, Post, Query } from '@nestjs/common';
import { CacheService } from './cache.service';

@Controller('cache')
export class CacheController {
  constructor(private readonly cacheService: CacheService) {}

  @Get('stakes')
  getStakes() {
    return this.cacheService.findAllStakes();
  }

  @Get('stakes/:account')
  getStakesByAccount(@Param('account') account: string) {
    return this.cacheService.findStakesByAccount(account);
  }

  @Post('forceupdate')
  async forceUpdate() {
    await this.cacheService.updateCache();
    return {
      updated: true,
    };
  }
}
