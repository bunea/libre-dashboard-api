import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { StakesService } from 'src/stakes/stakes.service';
import { VolumesService } from 'src/volumes/volumes.service';
import { Repository } from 'typeorm';
import { Stake } from './entities/stake.entity';
import { Turnover } from './entities/turnover.entity';

@Injectable()
export class CacheService {
  constructor(
    @InjectRepository(Stake)
    private readonly _stakeRepository: Repository<Stake>,
    private readonly _stakeService: StakesService,
    @InjectRepository(Turnover)
    private readonly _turnoverRepository: Repository<Turnover>,
  ) {}

  findAllStakes() {
    return this._stakeRepository.find();
  }

  findStakesByAccount(account: string) {
    return this._stakeRepository.find({
      where: {
        account: account,
      },
    });
  }

  async updateOne(index: number, data: any) {
    let stake = await this._stakeRepository.findOne({
      where: {
        index: index,
      },
    });
    if (!stake) {
      stake = this._stakeRepository.create();
      stake.index = index;
    }
    stake.account = data.account;
    stake.apy = data.apy;
    stake.libre_staked = data.libre_staked.replace(' LIBRE', '');
    stake.mint_bonus = data.mint_bonus;
    stake.payout = data.payout;
    stake.payout_date = data.payout_date;
    stake.stake_date = data.stake_date;
    stake.stake_length = data.stake_length;
    stake.status = data.status;
    await this._stakeRepository.save(stake);
    console.log('Updated stake with index: ', index);
  }

  // @Cron(CronExpression.EVERY_5_MINUTES)
  async updateCache() {
    const onChainStakes = await this._stakeService.findAll();
    for (const stake of onChainStakes) {
      await this.updateOne(stake.index, stake);
    }
  }

  async insertTurnover(
    date: string,
    pool: string,
    pool1: number,
    pool2: number,
  ) {
    const existing = await this._turnoverRepository.findOne({
      where: {
        date: date.substring(0, 10),
        pool,
      },
    });
    if (existing) {
      await this._turnoverRepository.remove(existing);
    }
    return this._turnoverRepository.save({
      date: date.substring(0, 10),
      pool,
      pool_1: pool1,
      pool_2: pool2,
    });
  }

  async getCachedTurnover(date: string, pool: string) {
    console.log(date);
    return this._turnoverRepository.findOne({
      where: {
        date: date.substring(0, 10),
        pool,
      },
    });
  }
}
