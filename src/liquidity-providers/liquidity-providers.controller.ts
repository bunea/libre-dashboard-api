import { Controller, Get, Param } from '@nestjs/common';
import { LiquidityProvidersService } from './liquidity-providers.service';

@Controller('liquidity-providers')
export class LiquidityProvidersController {
  constructor(
    private readonly liquidityProvidersService: LiquidityProvidersService,
  ) {}

  @Get(':account/balance')
  async getBalance(@Param('account') account: string) {
    const result = await this.liquidityProvidersService.findByAccount(account);
    return result;
  }
}
