import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { CHAIN_CONFIG } from 'src/core/config';

@Injectable()
export class StakesService {
  constructor(private readonly _httpService: HttpService) {}

  async findAll() {
    const data = [];
    let cursor = 0;
    const limit = 50;

    while (true) {
      const result = await this._httpService
        .post(`${CHAIN_CONFIG().hyperion.url}/v1/chain/get_table_rows`, {
          code: 'stake.libre',
          table: 'stake',
          scope: 'stake.libre',
          json: true,
          lower_bound: cursor,
          upper_bound: cursor + limit,
        })
        .toPromise();
      const rows = result.data.rows;
      console.log('invoked');
      console.log(rows);
      data.push(...rows);
      cursor += limit;
      if (rows.length === 0) {
        break;
      }
    }
    return data;
  }

  async findByAccount(account: string) {
    const result = await this._httpService
      .post(`${CHAIN_CONFIG().hyperion.url}/v1/chain/get_table_rows`, {
        code: 'stake.libre',
        table: 'stake',
        scope: 'stake.libre',
        json: true,
        index_position: 2,
        lower_bound: account,
        key_type: 'name',
        limit: 1000,
      })
      .toPromise();
    return result.data.rows.filter((row) => row.account === account);
  }

  async getBalance(account: string, token: string) {
    const data = await this._httpService
      .get(
        `${CHAIN_CONFIG().hyperion.url}/v2/state/get_tokens?account=${account}`,
      )
      .toPromise();
    const tokens = data.data.tokens.filter((c) => c.symbol == token);
    if (tokens && tokens.length) {
      return tokens[0].amount;
    } else {
      return 0;
    }
  }
}
