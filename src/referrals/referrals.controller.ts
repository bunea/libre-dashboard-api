import { HttpService } from '@nestjs/axios';
import { Controller, Get, Param } from '@nestjs/common';
import {
  ReferralLeaderBoardItem,
  ReferralStats,
  UserReferrees,
} from 'src/core/interfaces';
import { ReferralsService } from './referrals.service';

@Controller('referrals')
export class ReferralsController {
  constructor(private _referralsService: ReferralsService) {}

 

  @Get('leaderboard')
  async leaderBoard() {
    return this._referralsService.leaderBoard();
  }

  @Get('user/:name')
  async myReferrees(@Param('name') name: string) {
    return this._referralsService.myReferrees(name);
  }

  @Get('stats/:name')
  async userStats(@Param('name') name: string): Promise<ReferralStats> {
    return {
      count: 0,
      bitcoinEarning: 0,
      libreEarning: 0,
    };
  }
}
