import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { CHAIN_CONFIG } from 'src/core/config';

@Injectable()
export class WrappingService {
  constructor(private _httpService: HttpService) {}

  async getLightningInvoice(
    account: string,
    amount: number,
  ): Promise<string | null> {
    try {
      const invoiceResult = await this._httpService
        .post(`${CHAIN_CONFIG().core.url}/lightning-invoices`, {
          account,
          memo: '',
          amount,
          expiryInSeconds: 3600,
        })
        .toPromise();
      return invoiceResult.data.paymentRequest;
    } catch (e) {
      return null;
    }
  }

  async getBitcoinAddress(account: string) {
    try {
      const data = await this._httpService
        .get(`${CHAIN_CONFIG().core.url}/ptokens/ptoken-wrapping/${account}`)
        .toPromise();
      return data.data.depositAddress;
    } catch (e) {
      return null;
    }
  }
}
