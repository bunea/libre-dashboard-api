<div align="center">
<h1 align="center">
<img src="https://gitlab.com/uploads/-/system/group/avatar/55037359/Icon.png?width=64" width="100" />
<br>libre-dashboard-api
</h1>
<h3>◦ Unlock Your Data, Embrace Freedom</h3>
<h3>◦ Developed with the software and tools listed below.</h3>

<p align="center">
<img src="https://img.shields.io/badge/JavaScript-F7DF1E.svg?style&logo=JavaScript&logoColor=black" alt="JavaScript" />
<img src="https://img.shields.io/badge/Prettier-F7B93E.svg?style&logo=Prettier&logoColor=black" alt="Prettier" />
<img src="https://img.shields.io/badge/Jest-C21325.svg?style&logo=Jest&logoColor=white" alt="Jest" />
<img src="https://img.shields.io/badge/Webpack-8DD6F9.svg?style&logo=Webpack&logoColor=black" alt="Webpack" />
<img src="https://img.shields.io/badge/Axios-5A29E4.svg?style&logo=Axios&logoColor=white" alt="Axios" />
<img src="https://img.shields.io/badge/ESLint-4B32C3.svg?style&logo=ESLint&logoColor=white" alt="ESLint" />
<img src="https://img.shields.io/badge/SemVer-3F4551.svg?style&logo=SemVer&logoColor=white" alt="SemVer" />
<img src="https://img.shields.io/badge/Lodash-3492FF.svg?style&logo=Lodash&logoColor=white" alt="Lodash" />

<img src="https://img.shields.io/badge/tsnode-3178C6.svg?style&logo=ts-node&logoColor=white" alt="tsnode" />
<img src="https://img.shields.io/badge/TypeScript-3178C6.svg?style&logo=TypeScript&logoColor=white" alt="TypeScript" />
<img src="https://img.shields.io/badge/Ajv-23C8D2.svg?style&logo=Ajv&logoColor=white" alt="Ajv" />
<img src="https://img.shields.io/badge/Buffer-231F20.svg?style&logo=Buffer&logoColor=white" alt="Buffer" />
<img src="https://img.shields.io/badge/Express-000000.svg?style&logo=Express&logoColor=white" alt="Express" />
<img src="https://img.shields.io/badge/JSON-000000.svg?style&logo=JSON&logoColor=white" alt="JSON" />
<img src="https://img.shields.io/badge/Markdown-000000.svg?style&logo=Markdown&logoColor=white" alt="Markdown" />
</p>
</div>

---

## 📒 Table of Contents
- [📒 Table of Contents](#-table-of-contents)
- [📍 Overview](#-overview)
- [⚙️ Features](#-features)
- [📂 Project Structure](#project-structure)
- [🧩 Modules](#modules)
- [🚀 Getting Started](#-getting-started)
- [🗺 Roadmap](#-roadmap)
- [🤝 Contributing](#-contributing)
- [📄 License](#-license)
- [👏 Acknowledgments](#-acknowledgments)

---


## 📍 Overview

The Libre Dashboard API project is a Nest.js application that provides a range of functionalities for managing and retrieving data related to various aspects of the [Libre Dashboard](https://gitlab.com/libre-tech/libre-dashboard). It includes features for fetching and calculating statistics, retrieving information about producers and minting operations, handling referrals, and managing tokens. The project aims to provide a comprehensive API for monitoring and managing the system, making it easier for users to access and analyze data, and ultimately enhancing their overall experience and decision-making processes.

---

## ⚙️ Features

| Feature                | Description                           |
| ---------------------- | ------------------------------------- |
| **⚙️ Architecture**     | The codebase follows the Nest.js framework, which utilizes a modular and component-based architecture. It organizes the application into modules, each responsible for a specific set of functionalities. The modules are configured using decorators and can be easily plugged into the main application. The codebase also follows the MVC (Model-View-Controller) architectural pattern, where models represent the data structure, controllers handle HTTP requests, and services contain the business logic. This architecture promotes separation of concerns and maintainability. Additionally, the codebase implements a caching module and follows the repository pattern for database interactions, providing a clear separation between business logic and data access code. |
| **📖 Documentation**   | The codebase includes documentation in the form of code comments and separate files explaining the purpose and functionality of various components. The code comments are descriptive and provide insights into the purpose of the code, input/output parameters, and any important considerations. The presence of a Procfile suggests that the codebase may have been designed with production deployment in mind. However, the documentation could be improved by including higher-level documentation, such as an architecture overview and usage examples. |
| **🔗 Dependencies**    | The codebase relies on various external dependencies, including Nest.js, Express.js, and TypeORM. Nest.js is a powerful and popular framework for building scalable Node.js applications. Express.js is a minimal and flexible web application framework that enhances the routing capabilities of Nest.js. TypeORM is an ORM (Object-Relational Mapping) library that simplifies database operations by providing an abstraction layer over SQL databases. These dependencies enhance the development experience and provide robust tools for building feature-rich web applications. |
| **🧩 Modularity**      | The codebase demonstrates a high degree of modularity. It is divided into multiple modules, each encapsulating a specific set of features. These modules can be easily added or removed, enabling developers to extend or modify the application without impacting other modules. The codebase also defines services, controllers, and repositories that promote a modular and reusable code structure. This modularity allows for better code organization, maintainability, and testability. |
| **✔️ Testing**          | The codebase includes tests for end-to-end scenarios, such as the e2e test in the "test/app.e2e-spec.ts" file, which tests the application's behavior using actual HTTP requests. However, the codebase could benefit from additional unit tests for individual components and logic. The absence of comprehensive testing documentation makes it difficult to determine the level of test coverage and the testing strategies used. Incorporating more tests and documenting the testing approach would improve the overall quality and reliability of the codebase. |
| **⚡️ Performance**      | Performance considerations are not explicitly addressed in the provided codebase. However, the use of Nest.js, which is built

---


## 📂 Project Structure


```bash
repo
├── Procfile
├── README.md
├── nest-cli.json
├── package.json
├── src
│   ├── app.controller.ts
│   ├── app.module.ts
│   ├── app.service.ts
│   ├── cache
│   │   ├── cache.controller.ts
│   │   ├── cache.module.ts
│   │   ├── cache.service.ts
│   │   └── entities
│   │       └── stake.entity.ts
│   ├── core
│   │   ├── config.ts
│   │   ├── entities
│   │   │   └── base.entity.ts
│   │   ├── interfaces.ts
│   │   └── locationMap.ts
│   ├── main.ts
│   ├── mints
│   │   ├── mints.controller.ts
│   │   ├── mints.module.ts
│   │   └── mints.service.ts
│   ├── referrals
│   │   ├── referrals.controller.ts
│   │   ├── referrals.module.ts
│   │   └── referrals.service.ts
│   ├── stakes
│   │   ├── stakes.controller.ts
│   │   ├── stakes.module.ts
│   │   └── stakes.service.ts
│   ├── tokens
│   │   ├── tokens.controller.ts
│   │   ├── tokens.module.ts
│   │   └── tokens.service.ts
│   └── wrapping
│       ├── wrapping.controller.ts
│       ├── wrapping.module.ts
│       └── wrapping.service.ts
├── test
│   ├── app.e2e-spec.ts
│   └── jest-e2e.json
├── tsconfig.build.json
├── tsconfig.json
└── yarn.lock

12 directories, 36 files
```

---

## 🧩 Modules

<details closed><summary>Root</summary>

| File         | Summary                                                                                                                                                                                                                                                                                                                     |
| ---          | ---                                                                                                                                                                                                                                                                                                                         |
| .eslintrc.js | This code snippet exports an ESLint configuration object for TypeScript projects. It specifies the parser, parser options, plugins, rule sets, and environment settings for the ESLint tool. It disables certain rules related to TypeScript interfaces, function return types, module boundaries, and explicit'any' types. |
| Procfile     | The code snippet is a command used to start a web application in production mode. It runs the "start:prod" script defined in the npm package.json file, which likely includes the necessary build and server configurations for the application to be deployed and accessed by users.                                       |

</details>

<details closed><summary>Test</summary>

| File            | Summary                                                                                                                                                                                                                                                                                |
| ---             | ---                                                                                                                                                                                                                                                                                    |
| app.e2e-spec.ts | This code snippet is a test for an e2e (end-to-end) scenario for a Nest.js application. It sets up a testing module, creates an instance of the application, and then performs a GET request to the root path ("/") expecting a 200 status code and a response body of "Hello World!". |

</details>

<details closed><summary>Src</summary>

| File              | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ---               | ---                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| main.ts           | The code creates a NestJS server with CORS enabled. It generates API documentation using Swagger, specifying the title, description, and version. The server listens on the specified port or a default value.                                                                                                                                                                                                                                 |
| app.service.ts    | This code snippet provides an `AppService` class with four core functionalities:1. `getProducers()` retrieves a list of producers from an external API and calculates their rankings based on total votes.2. `getVotee(account: string)` retrieves the producer a particular account has voted for.3. `getChainSettings()` retrieves chain settings from an external API.4. `getChainStats()` retrieves chain statistics from an external API. |
| app.module.ts     | The code snippet defines the core functionalities of a Nest.js application. It includes various modules for handling HTTP requests, scheduling tasks, managing database connections, and caching data. It also imports controllers and services for the application.                                                                                                                                                                           |
| app.controller.ts | The code snippet defines an AppController class with four GET routes. It imports and uses the AppService class to handle these routes. The routes include fetching producers, fetching a voted account, fetching chain settings, and fetching chain statistics.                                                                                                                                                                                |

</details>

<details closed><summary>Core</summary>

| File           | Summary                                                                                                                                                                                                                                                                                                   |
| ---            | ---                                                                                                                                                                                                                                                                                                       |
| locationMap.ts | The code snippet defines a Map object that maps numeric keys to corresponding country names. The keys represent country codes and the values represent the names of the countries. The map is then exported for use in other modules.                                                                     |
| config.ts      | The code snippet defines a function that returns a configuration object based on the environment variable. It provides different values for hyperion and core URLs, as well as symbol, contract, and precision values for btc, usdt, and libre tokens, depending on the environment (development or not). |
| interfaces.ts  | The provided code snippet defines several interfaces that represent different sets of data related to minting, contributions, stakes, referrals, and account statistics. These interfaces define the structure of the data objects used in the code.                                                      |

</details>

<details closed><summary>Entities</summary>

| File            | Summary                                                                                                                                                                                                                                                                                                                                                                             |
| ---             | ---                                                                                                                                                                                                                                                                                                                                                                                 |
| base.entity.ts  | This code snippet provides a base entity class with core functionalities for database models. It includes an automatically generated UUID primary key, timestamp columns for creation and update dates.                                                                                                                                                                             |
| stake.entity.ts | This code snippet defines the Stake entity class using TypeORM. It has properties like index, account, stake_date, stake_length, mint_bonus, libre_staked, apy, payout, payout_date, and status. These properties are mapped to corresponding columns in the database table named "stakes". It extends a BaseEntity class and has a constructor to assign values to its properties. |

</details>

<details closed><summary>Cache</summary>

| File                | Summary                                                                                                                                                                                                                                                                                                                                             |
| ---                 | ---                                                                                                                                                                                                                                                                                                                                                 |
| cache.module.ts     | The code snippet defines a NestJS module called CacheModule that imports and uses various dependencies including a cache service, a cache controller, a TypeORM module for the Stake entity, a StakesModule, and a ScheduleModule. It also specifies the cache controller and service as providers for the module.                                  |
| cache.controller.ts | The code snippet is a NestJS controller that handles cache-related operations. It exposes three endpoints: 1. GET /cache/stakes-retrieves all stakes from the cache.2. GET /cache/stakes/:account-retrieves stakes by account from the cache.3. POST /cache/forceupdate-updates the cache and returns a success response.                           |
| cache.service.ts    | The provided code snippet is a NestJS CacheService module that interacts with a Stake repository. It provides functions to find stakes, update stakes, and update the stake cache periodically using a cron job. The cache update is performed by fetching stakes from an external service (StakesService) and updating the repository accordingly. |

</details>

<details closed><summary>Referrals</summary>

| File                    | Summary                                                                                                                                                                                                                                                                                                                        |
| ---                     | ---                                                                                                                                                                                                                                                                                                                            |
| referrals.service.ts    | The code snippet defines a ReferralsService class with two methods. The leaderBoard method retrieves data from an external API to get the dashboard users' statistics. The myReferrees method retrieves data for a specific user's statistics. The HttpService is used to make HTTP requests.                                  |
| referrals.module.ts     | The code snippet defines a module in a NestJS application for handling referrals. It imports the HttpModule from the'@nestjs/axios' package and injects it into the module. It also provides a ReferralsService for handling referral-related logic and a ReferralsController for handling HTTP requests related to referrals. |
| referrals.controller.ts | The code snippet defines a controller class for handling referral-related API endpoints. It has methods for getting a referral leaderboard, retrieving a user's referees, and fetching referral stats for a user. The methods utilize an injected service to handle the business logic.                                        |

</details>

<details closed><summary>Mints</summary>

| File                | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ---                 | ---                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| mints.service.ts    | This code snippet defines a service in a NestJS application that interacts with an external API. It provides methods to retrieve mint data, including all mints, mints by account, and mint statistics. The service uses the HttpService to make POST requests to the API endpoints and transforms the retrieved data as needed.                                                                                                                     |
| mints.controller.ts | This code snippet is for a NestJS controller called "MintsController". It contains three GET routes:1. "/mints/stats" retrieves mint statistics from the "MintsService".2. "/mints/:account/list" retrieves mints by account using the account parameter.3. "/mints/:account/stats" retrieves mint data for a specific account, including contributed amount, libre contributed, and percentage of contribution compared to overall mint statistics. |
| mints.module.ts     | The code snippet defines a module for minting functionality in a NestJS application. It imports the HttpModule for making HTTP requests, includes a MintsController for handling minting requests, and provides a MintsService for performing minting operations.                                                                                                                                                                                    |

</details>

<details closed><summary>Wrapping</summary>

| File                   | Summary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ---                    | ---                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| wrapping.service.ts    | The provided code snippet defines a service class called WrappingService. It uses the HttpService from the NestJS framework to make HTTP requests. The class has two methods:1. getLightningInvoice: accepts an account and amount, sends a POST request to a specified URL, and returns a lightning invoice payment request.2. getBitcoinAddress: accepts an account, sends a GET request to a specified URL, and returns a Bitcoin deposit address.Overall, the WrappingService provides functionality related to generating lightning invoices and retrieving Bitcoin deposit addresses. |
| wrapping.controller.ts | This code snippet defines a controller class in NestJS that handles HTTP GET requests for two specific routes: "/wrapping/bitcoin/:account" and "/wrapping/lightning/:account". The controller utilizes a WrappingService to retrieve a Bitcoin address for a given account and generate a Lightning Network payment invoice with a specified amount. The responses are returned as JSON objects.                                                                                                                                                                                           |
| wrapping.module.ts     | The code snippet defines a WrappingModule that imports HttpModule from the NestJS framework. It provides WrappingService as a provider and WrappingController as a controller within the module. This module encapsulates the core functionalities related to wrapping and allows for easy integration and management.                                                                                                                                                                                                                                                                      |

</details>

<details closed><summary>Stakes</summary>

| File                 | Summary                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ---                  | ---                                                                                                                                                                                                                                                                                                                                                                                                                      |
| stakes.controller.ts | The code snippet is a controller class for handling HTTP requests related to stakes. It has three endpoints: getAll, findByAccount, and getAggregate. The getAll endpoint returns all stakes, the findByAccount endpoint returns stakes specific to an account, and the getAggregate endpoint calculates aggregate stake, balance, and earned amount for a given account.                                                |
| stakes.module.ts     | This code snippet defines the StakesModule, which is responsible for importing necessary modules, providing a StakesService, defining a StakesController, and exporting the StakesService. The HttpModule is imported from'@nestjs/axios' to enable HTTP requests.                                                                                                                                                       |
| stakes.service.ts    | This code snippet defines a `StakesService` class that provides three core functionalities:1. `findAll()`: Fetches all stake data from an external API by making multiple requests in a loop.2. `findByAccount(account: string)`: Retrieves stake data specific to a given account from an API.3. `getBalance(account: string, token: string)`: Fetches the balance of a specific token for a given account from an API. |

</details>

<details closed><summary>Tokens</summary>

| File                 | Summary                                                                                                                                                                                                                                                                                                                                                                           |
| ---                  | ---                                                                                                                                                                                                                                                                                                                                                                               |
| tokens.module.ts     | The code snippet defines a module for handling tokens in a NestJS application. It imports and configures various dependencies including services, controllers, modules, and entities. The module provides the TokensService, TokensController, and CacheService, and is responsible for managing token-related functionality.                                                     |
| tokens.service.ts    | This code snippet is a module that provides various functionalities related to tokens in a cryptocurrency system. It includes methods for retrieving token prices, fetching tokens by account, getting currency stats, and calculating the remaining balance for a specific token. The module utilizes HTTP requests and dependencies such as the StakesService and CacheService. |
| tokens.controller.ts | This code snippet defines a controller for handling token-related requests in a NestJS application. It includes two API endpoints: one for retrieving all tokens and their information, and another for retrieving tokens associated with a specific account. The controller relies on the TokensService and CacheService for retrieving data and performing calculations.        |

</details>

---

## 🚀 Getting Started

### ✔️ Prerequisites

Before you begin, ensure that you have the following prerequisites installed:
> - `ℹ️ node v16+`
> - `ℹ️ postgres`
> - `ℹ️ yarn`

### 📦 Installation

1. Clone the libre-dashboard-api repository:
```sh
git clone /Users/b/Projects/Libre/Tech/libre-dashboard-api
```

2. Change to the project directory:
```sh
cd libre-dashboard-api
```

3. Install the dependencies:
```sh
yarn
```

### 🎮 Using libre-dashboard-api

### First Time

Copy the `.env.example` then update the environment variables according to your needs.

```
cp .env.example .env
```

Set your postgres user password in .env and set your local node instead of https://lb.libre.org for increased speed and decentralization

```
nano .env
```

Setup postgres user and database:

Authenticate to postgres cli
```bash
sudo -u postgres psql
```

Set the postgres password to whatever is in your .env

```sql
\password postgres
\q
```

Run the script to set up the postgres database based on your .env
```bash
./scripts/create_postgres.sh
```

Run development in watch mode:
```bash
yarn start:dev
```

Run production in watch mode:
```bash
yarn start:prod
```

Deployment - you might want to run nestjs on a custom port instead of 3000:
```bash
PORT=5000 pm2 start yarn --name "dashboard-api" -- start:prod -p 5000
```

Now you can add this as an upstream in nginx:
```bash
upstream dashboard-api {
    server 127.0.0.1:5000 fail_timeout=15s;
}
```

Next, set this as the server for your .env deployment of the [Libre Dashboard](https://gitlab.com/libre-tech/libre-dashboard)

### 🧪 Running Tests
```sh
yarn test
```

---

## Dockerfile Usage

This repository includes a Dockerfile that allows you to containerize and run the application.

### Prerequisites

Make sure you have Docker installed on your system. If you don't have Docker, you can download and install it from the official website: [Docker Official Website](https://www.docker.com/get-started).

### Building the Docker Image

To build the Docker image, navigate to the root of the project where the `Dockerfile` is located, and run the following command:

```sh
docker build -f Dockerfile -t your_image_name \
--build-arg DATABASE="postgresql://postgres:<db_name>@localhost:5432/libre_dashboard_db?sslmode=disable" \
--build-arg NODE_TLS_REJECT_UNAUTHORIZED="0" \
--build-arg NEXT_PUBLIC_LIBRE_API_ENDPOINT="https://lb.libre.org \
--build-arg NEXT_PUBLIC_LIBRE_CHAIN_ENDPOINT="production" \
--build-arg NEXT_PUBLIC_MARKUS_API_ENDPOINT="tls" \
.
```

## 🗺 Roadmap

> - [X] `ℹ️  Task 1: Implement X`
> - [ ] `ℹ️  Task 2: Refactor Y`
> - [ ] `ℹ️ ...`


---

## 🤝 Contributing

Contributions are always welcome! Please follow these steps:
1. Fork the project repository. This creates a copy of the project on your account that you can modify without affecting the original project.
2. Clone the forked repository to your local machine using a Git client like Git or GitHub Desktop.
3. Create a new branch with a descriptive name (e.g., `new-feature-branch` or `bugfix-issue-123`).
```sh
git checkout -b new-feature-branch
```
4. Make changes to the project's codebase.
5. Commit your changes to your local branch with a clear commit message that explains the changes you've made.
```sh
git commit -m 'Implemented new feature.'
```
6. Push your changes to your forked repository on GitHub using the following command
```sh
git push origin new-feature-branch
```
7. Create a new pull request to the original project repository. In the pull request, describe the changes you've made and why they're necessary.
The project maintainers will review your changes and provide feedback or merge them into the main branch.

---

## 📄 License

This project is licensed under the `ℹ️  INSERT-LICENSE-TYPE` License. See the [LICENSE](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/adding-a-license-to-a-repository) file for additional info.

---

## 👏 Acknowledgments

> - `ℹ️  List any resources, contributors, inspiration, etc.`

---
